CREATE TABLE IF NOT EXISTS llx_aide
(
  rowid integer NOT NULL,
  pathname VARCHAR(250) NOT NULL,
  content VARCHAR(2000) NOT NULL,
  PRIMARY KEY (rowid)
);

CREATE SEQUENCE IF NOT EXISTS public.llx_aide_rowid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.llx_aide_rowid_seq OWNER TO postgres;

ALTER SEQUENCE public.llx_aide_rowid_seq OWNED BY public.llx_aide.rowid;


INSERT INTO llx_aide(rowid, pathname, content)
 VALUES
 (1, '/index.php' , 'Des messages d''aide peuvent être ajoutés par page.'),
 (2, '/custom/aide/admin/setup.php' , 'Les messages d''aides apparaissent sur les pages correspondant au pathname renseigné.');

 SELECT setval('llx_aide_rowid_seq', 2, true);