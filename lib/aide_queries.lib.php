<?php

    function aide_fetchAll($db) {
        $sql = "SELECT * FROM llx_aide ORDER BY pathname";

        $resql = $db->query($sql);
        return $resql;
    }

    function aide_getByPathName($db, $pathname) {
        $sql = "SELECT * FROM llx_aide WHERE pathname like '" . $pathname . "'";

        $resql = $db->query($sql);
        return $resql;
    }

    function aide_update($db, $rowid, $pathname, $content) {
        $db->begin();
        $error = 0;

		$sql = "UPDATE llx_aide";
		$sql .= " SET pathname = '" . $pathname . "'";
		$sql .= " , content = '" . $content . "'";
		$sql .= " WHERE rowid = " . $rowid;


        $resql = $db->query($sql);
        if ($resql) {
            setEventMessages('L\'aide a correctement été modifiée.', null, 'mesgs');
        } else {
            $error++;
            dol_print_error($db);
        }

        if (!$error) {
            $db->commit();
        } else {
            $db->rollback();
        }
    }

    function aide_insert($db, $pathname, $content) {
        $db->begin();
        

        $sql = "INSERT INTO llx_aide";
		$sql .= " (rowid, pathname, content) ";
		$sql .= " values (nextval('llx_aide_rowid_seq'), '". $pathname . "' , '" . $content . "') ";

        $resql = $db->query($sql);
        
        if ($resql) {
            setEventMessages('L\'aide a été ajoutée.', null, 'mesgs');
        } else {
            $error++;
            dol_print_error($db);
        }

        if (!$error) {
            $db->commit();
        } else {
            $db->rollback();
        }
    }

    function aide_delete($db, $rowid) {
        $db->begin();
        

        $sql = "DELETE FROM llx_aide";
		$sql .= " where rowid = ". $rowid ;

        $resql = $db->query($sql);

        if ($resql) {
            setEventMessages('L\'aide a été supprimée.', null, 'mesgs');
        } else {
            $error++;
            dol_print_error($db);
        }

        if (!$error) {
            $db->commit();
        } else {
            $db->rollback();
        }
    }
?>