function toggleAidePanel() {
    let panel = document.getElementById("aide-panel");
    if (panel) {
        panel.classList.toggle("opened");
    }
}