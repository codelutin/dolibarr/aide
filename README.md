#  🇬🇧 HELP MODULE FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## Features

This module lets you add notes to certain Dolibarr screens.

---

# 🇫🇷 MODULE AIDE POUR L'[ERP DOLIBARR](https://www.dolibarr.org)

## Fonctionnalités

Ce module permet d'ajouter des notes sur certains écrans de Dolibarr.
